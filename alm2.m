totalN =round(sqrt(2419*2));

Nctheta =totalN;
hcTheta = 2.0/Nctheta;

Nphi = totalN;
hphi = 2*pi/Nphi;
ctheta = linspace(-1,1,Nctheta);
phi = linspace(hphi,2*pi,Nphi);

sd =sqrt(103.100*2*pi/(2419*2419)); % in unit of muK sd =sqrt(103.100*2*pi/(2419*2419)); % in unit of muK
dT =normrnd(0,sd,Nctheta,Nphi);

lhigh =tblread('cmb_high.txt');
lmax = max(lhigh(:,1))
l =lhigh(1:end,1)
Cl = zeros(1,lmax);

parfor li = l'
m = (0:li);
result = zeros(1,li);
parfor mi = m
       result(mi+1) = abs(sum(sum((conj(sphericalHarmonics2(li,mi,ctheta,phi)).*dT'))*hphi*hcTheta))^2;
end
Cl(li) = (2*sum(result(2:end))+result(1))/(2*li+1);
end

Cl = Cl(l)';
Dl = Cl.*(l.*(l+1))/(2*pi);
% Tcmb = 2.72548*10^6;
Dlt = lhigh(:,4);
Clt = Dlt./(l.*(l+1))*2*pi;
CE = -sum(Clt.*log(Clt./Cl));

