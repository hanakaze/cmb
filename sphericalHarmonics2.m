function sphH = sphericalHarmonics2(l,m,ctheta,phi)
        sphH = legendre(l,ctheta,'norm');
        sphH = sphH(m+1,:)'*exp(1i*m*phi);
end