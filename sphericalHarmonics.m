function sphH = sphericalHarmonics(l,m,theta,phi)
        sphH = legendre(l,cos(theta),'norm');
        sphH = exp(1i*phi*m).*sphH(m+1,:);
end