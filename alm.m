Nside = floor(16); % sqrt(2*1536/12) corresponding to 554.480, l =1536
grid = pix2ang(Nside);
Npix =length(grid);
theta_phi = zeros(2,Npix);

for i=1:Npix
theta_phi(1,i) = grid{i}(1);
theta_phi(2,i) = grid{i}(2);
end


sd =sqrt(554.480*2*pi/(1536*1537)); % in unit of muK
dT =normrnd(0,sd,1,Npix);

lhigh =tblread('cmb_high.txt');
lmax = max(lhigh(:,1))
l =lhigh(1:end,1)
Cl = zeros(1,lmax);

for li = l'
m = (0:li);
result = zeros(1,li);
parfor mi = m
       result(mi+1) = abs(sum(sum((conj(sphericalHarmonics(li,mi,theta_phi(1,:),theta_phi(2,:))).*dT))*4*pi/Npix))^2;
end
Cl(li) = (2*sum(result(2:end))+result(1))/(2*li+1);
end

save('heal.mat');
quit();

Cl = Cl(l)';
Dl = Cl.*(l.*(l+1))/(2*pi);
% Tcmb = 2.72548*10^6;
Dlt = lhigh(:,4);
Clt = Dlt./(l.*(l+1))*2*pi;
CE = -sum(Clt.*log(Clt./Cl));
